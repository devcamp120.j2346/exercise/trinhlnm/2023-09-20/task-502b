// Khai báo lớp
class Car {
    // Khai báo thuộc tính
    _name;
    _year;

    // Khai báo phương thức khởi tạo
    constructor(paramName, paramYear) {
        this._name = paramName;
        this._year = paramYear;
    }

    // Phương thức Getter
    getName() {
        return this._name;
    }

    getYear() {
        return this._year;
    }

    // Phương thức Setter
    setName(paramName) {
        this._name = paramName;
    }

    setYear(paramYear) {
        this._year = paramYear;
    }

    // Phương thức khác
    getAge() {
        let today = new Date().getFullYear();

        return today - this._year;
    }
}

export { Car }