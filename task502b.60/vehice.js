class Vehice {
    _brand;
    _yearManufactured;

    constructor(paramBrand = "Brand", paramYear = "2023") {
        this._brand = paramBrand;
        this._yearManufactured = paramYear;
    }

    getBrand() {
        return this._brand;
    }

    getYearManufactured() {
        return this._yearManufactured;
    }

    setBrand(paramBrand) {
        this._brand = paramBrand;
    }

    setYearManufactured(paramYear) {
        this._yearManufactured = paramYear;
    }

    print() {
        return this._brand + ", " + this._yearManufactured;
    }
}

export {Vehice}
