import {Vehice} from "./vehice.js";

class Car extends Vehice {
    _vId;
    _modelName;

    constructor(paramVId, paramModelName) {
        super();
        this._vId = paramVId;
        this._modelName = paramModelName;
    }

    getVId() {
        return this._vId;
    }

    getModelName() {
        return this._modelName;
    }

    setVId(paramVId) {
        this._vId = paramVId;
    }

    setModelName(paramModelName) {
        this._modelName = paramModelName;
    }

    honk() {
        console.log(this._modelName + " honking");
    }
}

export {Car}
