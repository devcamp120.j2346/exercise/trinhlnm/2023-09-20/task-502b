import { Vehice } from "./vehice.js";
import { Car } from "./car.js";
import { Motorbike } from "./motorbike.js";

var vehice1 = new Vehice("Toyota", 2019);
console.log(vehice1.print());
console.log(vehice1 instanceof Vehice);

var car1 = new Car("C1", "Vinfast");
car1.honk();
car1.setVId("C11");
console.log(car1.getVId());
car1.setModelName("Lexus");
console.log(car1.getModelName());
console.log(car1 instanceof Car);
console.log(car1 instanceof Vehice);
console.log(car1.print());

var motorbike1 = new Motorbike("M1", "Future");
console.log(motorbike1.getVId());
console.log(motorbike1.getModelName());
motorbike1.honk();
console.log(motorbike1.print());
console.log(motorbike1 instanceof Motorbike);
console.log(motorbike1 instanceof Vehice);
console.log(motorbike1 instanceof Car);